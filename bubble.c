#include "bubble.h"

void ordenaAsc( int arr [], int arrSize ) {
    imprimirArr(arr, arrSize);

    int aux;
    int mod = 1;

    // iteracion 1
    // [ 3 2 1 ]
    // 3 > 2 ?
    // aux = 2
    // [ 3 3 1 ]
    // [ aux 3 1] = [ 2 3 1 ]

    // iteracion 2
    // [ 2 3 1 ]
    // 3 > 1 ?
    // aux = 1
    // [ 2 3 3 ]
    // [ 2 aux 3] = [ 2 1 3 ]

    while (mod) {
        mod = 0;
        for (int i = 0; i < arrSize - 1; i++) {
            if (arr[i] > arr[i+1]) {
                aux = arr[i+1];
                arr[i+1] = arr[i];
                arr[i] = aux;
                mod = 1;
            }
        }
    }
    imprimirArr(arr, arrSize);
}

void ordenaDesc( int arr [], int arrSize ) {
    imprimirArr(arr, arrSize);
    int aux;
    int mod = 1;
    while (mod) {
        mod = 0;
        for (int i = 0; i < arrSize - 1; i++) {
            if (arr[i] < arr[i+1]) {
                aux = arr[i+1];
                arr[i+1] = arr[i];
                arr[i] = aux;
                mod = 1;
            }
        }
    }
    imprimirArr(arr, arrSize);
}

void imprimirArr(  int arr [], int arrSize  ) {
    for (int i = 0; i < arrSize; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    
}