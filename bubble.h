#if !defined(BUBBLEH)
#define BUBBLEH

#include <stdio.h>

/*
*   Ordena ascendemente el arreglo que recibe en arr
*/
void ordenaAsc( int arr [], int arrSize );

/*
*   Ordena descendemente el arreglo que recibe en arr
*/
void ordenaDesc( int arr [], int arrSize);

void imprimirArr( int arr [], int arrSize);

#endif // BUBBLEH
