#include <stdio.h>
#include <string.h>

#include "bubble.h"

int main(int argc, char const *argv[])
{
    // recibir únicamente un argumento!!!
    // si ese argumento es "d" => ordenar descendentemente
    // si ese argumento es "a" => ordenar ascendentemente
    // hacemos bubble sort del siguiente arreglo:

    int arr [] = {2, 5, 1, 10, 12, 8, 6, 0 , 3, 14, 1}; // arrSize = 11

    // argv -> ["./bubble", "d"] -> argc (?) => 2 ✅
    // argv -> ["./bubble", "a"] -> argc (?) => 2 ✅

    if ( argc == 2 ) {
        // regresa 0 si son iguales
        if (strcmp(argv[1], "a") == 0) {
            // ascendentemente
            printf("Asc\n");
            ordenaAsc(arr, 11);
        }

        else if (strcmp(argv[1], "d") == 0) {
            // descendentemente
            printf("Desc\n");
            ordenaDesc(arr, 11);
        }

        else {
            printf("ERROR: orden no reconocido\n");
        }

    } else {
        printf("ERROR: Mas o menos argumentos de lo requerido\n");
    }

    return 0;
}
